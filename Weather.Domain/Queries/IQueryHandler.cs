﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace Weather.Domain.Queries
{
    public interface IQueryHandler<in TQuery, TResponse> : IRequestHandler<TQuery, TResponse>
           where TQuery : IQuery<TResponse>
    {
    }
}
