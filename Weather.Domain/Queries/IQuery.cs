﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace Weather.Domain.Queries
{
    public interface IQuery<out TResponse> : IRequest<TResponse>
    {
    }
}
