﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Weather.Domain.Queries
{
    public interface IQueryBus
    {
        Task<TResponse> Send<TQuery, TResponse>(TQuery query) where TQuery : IQuery<TResponse>;
    }
}
