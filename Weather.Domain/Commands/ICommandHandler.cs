﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace Weather.Domain.Commands
{
    public interface ICommandHandler<in T> : IRequestHandler<T>
        where T : ICommand
    {
    }
}
