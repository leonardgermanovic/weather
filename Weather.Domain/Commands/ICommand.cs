﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace Weather.Domain.Commands
{
    public interface ICommand : IRequest { }
}
