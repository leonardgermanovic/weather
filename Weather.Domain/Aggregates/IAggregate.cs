﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Weather.Domain.Aggregates
{
    public interface IAggregate
    {
        Guid Id { get; }
        int Version { get; }
    }
}
