﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Weather.Domain.Events
{
    public abstract class EventSourcedAggregate : IEventSourcedAggregate
    {
        public Guid Id { get; protected set; }

        public int Version { get; protected set; } = -1;

        protected int GetNextVersion() => Version + 1;

        public Queue<IEvent> PendingEvents { get; private set; }

        protected EventSourcedAggregate()
        {
            PendingEvents = new Queue<IEvent>();
        }

        protected void Append(IEvent @event)
        {
            PendingEvents.Enqueue(@event);
        }
    }
}
