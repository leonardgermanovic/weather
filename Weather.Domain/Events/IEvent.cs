﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace Weather.Domain.Events
{
    public interface IEvent : INotification
    {
    }
}
