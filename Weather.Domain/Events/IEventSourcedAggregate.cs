﻿using System;
using System.Collections.Generic;
using System.Text;
using Weather.Domain.Aggregates;

namespace Weather.Domain.Events
{
    public interface IEventSourcedAggregate : IAggregate
    {
        Queue<IEvent> PendingEvents { get; }
    }
}
