﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Weather.Domain.Events
{
    public interface IEventBus
    {
        Task Publish<TEvent>(params TEvent[] events) where TEvent : IEvent;
    }
}
