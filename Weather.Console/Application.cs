﻿using System.Linq;
using System.Collections.Generic;
using Weather.Application.Formatters;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Weather.BackendApi.Client.OpenAPIService;
using System;
using EvtSource;
using Weather.Application;
using System.Globalization;
using System.Threading;
using System.Net.Http;

namespace Weather.Console
{
    public class Application : IDisposable
    {
        private readonly IOutput _output;
        private readonly IFormatter<CityWeatherViewModel> _formatter;
        private readonly ILogger<Application> _logger;
        private readonly IClient _client;
        private readonly Settings _settings;

        private EventSourceReader _eventSourceReader;

        public Application(IOutput output, IFormatter<CityWeatherViewModel> formatter, ILogger<Application> logger, IClient client, Settings settings)
        {
            _output = output;
            _formatter = formatter;
            _logger = logger;
            _client = client;
            _settings = settings;
        }

        public async Task Run(List<string> cities, CancellationToken cancellationToken)
        {
            var apiCities = await _client.CitiesAsync(cities, cancellationToken).ConfigureAwait(false);
            var weathers = new List<CityWeatherViewModel>();
            foreach (var city in apiCities)
            {
                _logger.LogInformation("Quering {city}", city.Name);
                var weather = await _client.CityWeatherAsync(city.Id).ConfigureAwait(false);
                if (weather != null)
                {
                    //_output.Write(_formatter.Format(weather));
                    weathers.Add(weather);
                    WriteCityweather(weather);
                }
                else
                {
                    _logger.LogWarning("Weather for city {city} not found in backend", city);
                }
            }
            await Task.Delay(_settings.SchedulerPeriod, cancellationToken).ConfigureAwait(false);
            var id = string.Join("&", weathers.Select(x => $"id={x.Id}"));
            var v = string.Join("&", weathers.Select(x => $"v={x.Version}"));

            _eventSourceReader = new EventSourceReader(new Uri(_settings.BackendApiUrl + $"/api/sse?{id}&{v}")).Start();
            _logger.LogInformation("Quering events");
            _eventSourceReader.MessageReceived += (object sender, EventSourceMessageEventArgs e) =>
            {
                if (_eventSourceReader == null || _eventSourceReader.IsDisposed || cancellationToken.IsCancellationRequested)
                {
                    return;
                }
                if (e.Event == "message")
                {
                    //byte[] decodedBytes = Convert.FromBase64String(e.Message);
                    //string decodedTxt = System.Text.Encoding.UTF8.GetString(decodedBytes);
                    //string decodedTxt2 = System.Text.Encoding.Unicode.GetString(decodedBytes);
                    var model = Utf8Json.JsonSerializer.Deserialize<CityWeatherViewModel>(e.Message);
                    WriteCityweather(model, e.Event);
                }
                else
                {
                    _output.Write($"{e.Event} : {e.Message}");
                }
            };
            _eventSourceReader.Disconnected += async (object sender, DisconnectEventArgs e) => {
                _output.Write($"Retry: {e.ReconnectDelay} - Error: {e.Exception.Message}");
                try
                {
                    await Task.Delay(e.ReconnectDelay, cancellationToken);
                }
                catch (TaskCanceledException)
                {
                    _logger.LogInformation("Task was cancelled Application");
                }
                if (_eventSourceReader != null && !_eventSourceReader.IsDisposed && !cancellationToken.IsCancellationRequested)
                {
                    _eventSourceReader.Start(); // Reconnect to the same URL
                }
            };

            //foreach (var city in citiesWithId)
            //{
            //    _logger.LogInformation("Quering {city}", city.Name);
            //    var weather = await _client.ApiCityweatherAsync(city.Id).ConfigureAwait(false);
            //    if (weather != null)
            //    {
            //        _output.Write(_formatter.Format(weather));
            //    }
            //    else
            //    {
            //        _logger.LogWarning("Weather for city {city} not found in backend", city);
            //    }
            //}
        }

        private void WriteCityweather(CityWeatherViewModel model, string eventType = "message")
        {
            var delay = DateTime.UtcNow.Subtract(model.UpdatedAt.UtcDateTime);
            var updatedAt = model.UpdatedAt.UtcDateTime.ToString("yyyy-MM-dd HH:mm:ss.fffffff", CultureInfo.InvariantCulture);
            var message = $"City={model.City}, Temperature={model.Temperature}, UtdatedAt={updatedAt}, Delay={delay}, v={model.Version}, s={model.Source}";
            _output.Write($"{eventType} : {message}");
        }

        public void Dispose()
        {
            if (_eventSourceReader != null && !_eventSourceReader.IsDisposed)
            {
                _eventSourceReader.Dispose();
                _eventSourceReader = null;
            }
        }
    }
}
