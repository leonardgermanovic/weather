﻿using Autofac;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Serilog.Events;
using System;
using System.Collections.Generic;
using Weather.Application;
using Weather.WeatherApi.Client.OpenAPIService;
using Weather.Persistence;
using Weather.Persistence.Data;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Http;

namespace Weather.Console
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                var cities = new CommandLineParser().ParseArguments(args);
                var settings = new SettingsProvider().Load<Settings>(@"..\..\..\..\Weather.Application\settings.yaml");

                Services.RegisterServices(settings);
                var logger = (ILogger<Program>)Services.ServiceProvider.GetService(typeof(ILogger<Program>));
                logger.LogInformation($"Settings loaded with BackendApiUrl={settings.BackendApiUrl}");
                try
                {
                    using (var app = (Application)Services.ServiceProvider.GetService(typeof(Application)))
                    using (var cancellationTokenSource = new CancellationTokenSource())
                    {
                        try
                        {
                            logger.LogInformation($"Getting events for cities {string.Join(',', cities)}");
                            var appTask = app.Run(cities, cancellationTokenSource.Token);
                            appTask.Wait();
                            System.Console.ReadLine();
                            cancellationTokenSource.Cancel();
                            logger.LogInformation("Cancellation token canceled Program");
                        }
                        catch (TaskCanceledException)
                        {
                            logger.LogInformation("Task was cancelled Program");
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, "Exception in Main");
                }
                finally
                {
                    Services.DisposeServices();
                }
            }
            catch (Exception e)
            {
                System.Console.WriteLine($"There was an exception: {e.ToString()}");
            }
            System.Console.ReadLine();
        }
    }
}
