﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper.Contrib.Autofac.DependencyInjection;
using MediatR;
using MediatR.Extensions.Autofac.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Serilog.Events;
using Serilog.Extensions.Autofac.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Weather.Application;
using Weather.Application.CityWeathers.Queries.GetCityWeather;
using Weather.Application.Formatters;
using Weather.Persistence;
using Weather.Persistence.Data;
using Weather.Persistence.Infrastructure;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Http;
using System.Net.Http;
using System.Net;
using Weather.Domain.Queries;
using Weather.Domain.Commands;
using Weather.BackendApi.Client.OpenAPIService;
using CityWeatherViewModel = Weather.BackendApi.Client.OpenAPIService.CityWeatherViewModel;

namespace Weather.Console
{
    public class Services
    {
        public static IServiceProvider ServiceProvider { get; private set; }
        private static IHttpClientFactory _HttpClientFactory { get; set; }
        public static void RegisterServices(Settings settings)
        {
            var factoryCollection = new ServiceCollection();
            factoryCollection.AddHttpClient<Client>()
                .ConfigureHttpClient((sp, httpClient) =>
                {
                    httpClient.Timeout = TimeSpan.FromSeconds(30);
                })
                .SetHandlerLifetime(TimeSpan.FromMinutes(10))
                .ConfigurePrimaryHttpMessageHandler(x => new HttpClientHandler() { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate });
                //.AddHttpMessageHandler(sp => sp.GetService<AuthenticationHandlerFactory>().CreateAuthHandler())
                //.AddPolicyHandlerFromRegistry(PollyPolicyName.HttpRetry)
                //.AddPolicyHandlerFromRegistry(PollyPolicyName.HttpCircuitBreaker);
            var factoryBuilder = new ContainerBuilder();
            factoryBuilder.Populate(factoryCollection);
            var factoryContainer = factoryBuilder.Build();
            _HttpClientFactory = factoryContainer.Resolve<IHttpClientFactory>();

            var builder = new ContainerBuilder();
            builder.RegisterType<Application>();
            builder.RegisterInstance<Settings>(settings);
            //builder.RegisterType<HttpClient>();
            //builder.RegisterType<WeatherDataProviderFake>().As<IClient>();
            builder.Register<Client>(c => new Client(settings.BackendApiUrl, _HttpClientFactory.CreateClient("Client"))).As<IClient>();
            builder.RegisterType<ConsoleOutput>().As<IOutput>();

            var loggerConfiguration = new LoggerConfiguration()
                .Enrich.WithThreadId()
                .MinimumLevel.Debug()
                .WriteTo.Console(
                    LogEventLevel.Verbose,
                    "{Timestamp:HH:mm:ss} [{Level}] <{ThreadId}> {Message}{NewLine}{Exception}");
            builder.RegisterSerilog(loggerConfiguration);

            var dbContextFactory = new WeatherDbContextFactory();
            builder.Register(c => dbContextFactory.Create(settings.WeatherDatabase));

            builder.RegisterType<UnitOfWork<WeatherDbContext>>().As<IUnitOfWork<WeatherDbContext>>();
            builder.RegisterType<UnitOfWork<WeatherDbContext>>().As<IUnitOfWork>();
            builder.RegisterType<CityWeatherFormatter>().As<IFormatter<CityWeatherViewModel>>();
            builder.RegisterType<WeatherDataProviderWrapper>().As<IWeatherDataProviderWrapper>();
            builder.RegisterType<QueryBus>().As<IQueryBus>();
            builder.RegisterType<CommandBus>().As<ICommandBus>();
            builder.AddMediatR(typeof(GetCityWeatherQuery).Assembly);
            builder.AddAutoMapper(typeof(WeatherProfile).Assembly);

            var collection = new ServiceCollection();
            builder.Populate(collection);
            var appContainer = builder.Build();
            ServiceProvider = new AutofacServiceProvider(appContainer);
        }
        public static void DisposeServices()
        {
            if (_HttpClientFactory != null)
            {
                _HttpClientFactory = null;
            }

            if (ServiceProvider == null)
            {
                return;
            }

            if (ServiceProvider is IDisposable)
            {
                ((IDisposable)ServiceProvider).Dispose();
            }
        }
    }
}
