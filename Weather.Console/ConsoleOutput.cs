﻿using Microsoft.Extensions.Logging;

namespace Weather.Console
{
    public interface IOutput
    {
        void Write(string content);
    }

    public class ConsoleOutput : IOutput
    {
        private readonly ILogger<Application> _logger;

        public ConsoleOutput(ILogger<Application> logger)
        {
            _logger = logger;
        }

        public void Write(string content)
        {
            //System.Console.WriteLine(content);
            _logger.LogInformation(content);
        }
    }
}
