﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Weather.Application.Entities;

namespace Weather.Persistence.Configurations
{
    public class CityWeatherConfiguration : IEntityTypeConfiguration<CityWeather>
    {
        public void Configure(EntityTypeBuilder<CityWeather> builder)
        {
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id)
                .IsRequired();

            builder.Property(e => e.Weather)
                .HasMaxLength(64);

            builder.HasOne(e => e.City)
                .WithMany(p => p.CityWeathers)
                .HasForeignKey(d => d.CityId)
                .HasConstraintName("FK_CityWeathers_Cities");

            builder.Ignore(e => e.PendingEvents);
        }
    }
}
