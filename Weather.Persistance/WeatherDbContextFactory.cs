﻿using Microsoft.EntityFrameworkCore;
using Weather.Persistence.Infrastructure;

namespace Weather.Persistence
{
    public class WeatherDbContextFactory : DesignTimeDbContextFactoryBase<WeatherDbContext>
    {
        protected override WeatherDbContext CreateNewInstance(DbContextOptions<WeatherDbContext> options)
        {
            return new WeatherDbContext(options);
        }
    }
}
