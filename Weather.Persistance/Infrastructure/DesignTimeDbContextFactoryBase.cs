﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using Weather.Application;

namespace Weather.Persistence.Infrastructure
{
    public abstract class DesignTimeDbContextFactoryBase<TContext> :
        IDesignTimeDbContextFactory<TContext> where TContext : DbContext
    {
        public TContext CreateDbContext(string[] args)
        {
            //var basePath = Directory.GetCurrentDirectory() + string.Format("{0}..{0}Weather.Application", Path.DirectorySeparatorChar); 
            var settings = new SettingsProvider().Load<Settings>(@"..\..\..\..\Weather.Application\settings.yaml");
            var connectionString = settings.WeatherDatabase;
            return Create(connectionString);
        }

        protected abstract TContext CreateNewInstance(DbContextOptions<TContext> options);

        public TContext Create(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
            {
                throw new ArgumentException($"Connection string is null or empty.", nameof(connectionString));
            }

            var optionsBuilder = new DbContextOptionsBuilder<TContext>();
            optionsBuilder.UseSqlite(connectionString);
            return CreateNewInstance(optionsBuilder.Options);
        }
    }
}
