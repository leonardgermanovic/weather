﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Weather.BackendApi.Dto
{
    public class CityDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
