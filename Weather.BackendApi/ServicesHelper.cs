﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using MediatR;
using MediatR.Extensions.Autofac.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Serilog.Events;
using Serilog.Extensions.Autofac.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Weather.Application;
using Weather.Application.Formatters;
using Weather.Persistence;
using Weather.Persistence.Data;
using Weather.Persistence.Infrastructure;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Http;
using System.Net.Http;
using System.Net;
using Weather.Domain.Queries;
using Weather.Domain.Commands;
using Weather.WeatherApi.Client.OpenAPIService;
using WeatherApiClient = Weather.WeatherApi.Client.OpenAPIService.Client;
using Weather.Application.CityWeathers.Queries.GetCityWeather;
using Weather.Domain.Events;
using Weather.Application.CityWeathers.Events;
using Weather.BackendApi.Controllers;
using Serilog.Exceptions;
using Serilog.Sinks.Elasticsearch;

namespace Weather.BackendApi
{
    public static class ServicesHelper
    {
        private static IHttpClientFactory _HttpClientFactory { get; set; }
        public static void AddServices(this ContainerBuilder builder, Settings settings)
        {
            var factoryCollection = new ServiceCollection();
            factoryCollection.AddHttpClient<WeatherApiClient>()
                .ConfigureHttpClient((sp, httpClient) =>
                {
                    httpClient.Timeout = TimeSpan.FromSeconds(30);
                })
                .SetHandlerLifetime(TimeSpan.FromMinutes(10))
                .ConfigurePrimaryHttpMessageHandler(x => new HttpClientHandler() { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate });
                //.AddHttpMessageHandler(sp => sp.GetService<AuthenticationHandlerFactory>().CreateAuthHandler())
                //.AddPolicyHandlerFromRegistry(PollyPolicyName.HttpRetry)
                //.AddPolicyHandlerFromRegistry(PollyPolicyName.HttpCircuitBreaker);
            var factoryBuilder = new ContainerBuilder();
            factoryBuilder.Populate(factoryCollection);
            var factoryContainer = factoryBuilder.Build();
            _HttpClientFactory = factoryContainer.Resolve<IHttpClientFactory>();

            builder.RegisterType<Scheduler>();
            builder.RegisterInstance<Settings>(settings);
            //builder.RegisterType<HttpClient>();
            //builder.RegisterType<WeatherDataProviderFake>().As<IClient>();
            builder.Register<WeatherApiClient>(c => new WeatherApiClient(settings.WeatherApiUrl, _HttpClientFactory.CreateClient("Client"))).As<IClient>();
            //builder.RegisterType<ConsoleOutput>().As<IOutput>();

            var elasticSearchUri = "http://localhost:9200/";
            var loggerConfiguration = new LoggerConfiguration()
                .Enrich.WithThreadId()
                .Enrich.WithExceptionDetails()
                .MinimumLevel.Debug()
                //.WriteTo.Console(
                //    LogEventLevel.Verbose,
                //    "{Timestamp:HH:mm:ss} [{Level}] <{ThreadId}> {Message}{NewLine}{Exception}");
                //.WriteTo.File("log.txt",
                //    LogEventLevel.Information,
                //    "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] <{ThreadId}> {Message:lj}{NewLine}{Exception}",
                //    rollingInterval: RollingInterval.Day);
                .WriteTo.Elasticsearch(new ElasticsearchSinkOptions(new Uri(elasticSearchUri))
                {
                    AutoRegisterTemplate = true,
                });
            builder.RegisterSerilog(loggerConfiguration);

            var dbContextFactory = new WeatherDbContextFactory();
            builder.Register(c => dbContextFactory.Create(settings.WeatherDatabase));

            builder.RegisterType<UnitOfWork<WeatherDbContext>>().As<IUnitOfWork<WeatherDbContext>>();
            builder.RegisterType<UnitOfWork<WeatherDbContext>>().As<IUnitOfWork>();
            //builder.RegisterType<CityWeatherFormatter>().As<IFormatter<CityWeatherViewModel>>();
            builder.RegisterType<WeatherDataProviderWrapper>().As<IWeatherDataProviderWrapper>();
            builder.AddMediatR(typeof(GetCityWeatherQuery).Assembly);
            
            builder.RegisterType<QueryBus>().As<IQueryBus>();
            builder.RegisterType<CommandBus>().As<ICommandBus>();
            builder.RegisterType<EventBus>().As<IEventBus>();
            //builder.RegisterType<CityWeatherEventHandler>().As<INotificationHandler<CityWeatherTemperatureUpdated>>();
            builder.RegisterType<CityWeatherEventHandler>().As<INotificationHandler<CityWeatherCreated>>();
            builder.RegisterType<CityWeatherEventHandler>().As<INotificationHandler<CityWeatherUpdated>>();

            //builder.AddAutoMapper(typeof(WeatherProfile).Assembly);

            //builder.Populate(services);
            //var appContainer = builder.Build();
            //ServiceProvider = new AutofacServiceProvider(appContainer);
        }
    }
}
