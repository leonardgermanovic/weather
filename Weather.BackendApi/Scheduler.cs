﻿using MediatR;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Weather.Application;
using Weather.Application.CityWeathers.Queries.GetCityWeather;
using System.Threading.Tasks;
using Weather.Application.Cities.Commands.AddCity;
using Weather.Application.Cities.Commands.AddCityWeather;
using Weather.Domain.Queries;
using Weather.Domain.Commands;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Hosting;
using System.Threading;
using Weather.Domain.Events;
using Weather.Application.CityWeathers.Events;
using System.Collections.Concurrent;
using Weather.Application.Entities;
using Weather.Application.Cities.Commands.UpdateCityWeather;

namespace Weather.BackendApi
{
    public class Scheduler : IHostedService, IDisposable
    {
        private readonly IQueryBus _queryBus;
        private readonly ICommandBus _commandBus;
        private readonly IWeatherDataProviderWrapper _wrapper;
        private readonly ILogger _logger;
        private readonly Settings _settings;

        private Timer _timer;
        private int executionCount = 0;
        private bool _executing;

        public static Scheduler Instance { get; private set; }

        public event Action<CityWeatherTemperatureUpdated> OnTemperatureUpdated;

        public Scheduler(IQueryBus queryBus, ICommandBus commandBus, IWeatherDataProviderWrapper wrapper, ILogger<Scheduler> logger, Settings settings)
        {
            _queryBus = queryBus;
            _commandBus = commandBus;
            _wrapper = wrapper;
            _logger = logger;
            _settings = settings;
            Instance = this;
        }

        public void TemperatureUpdated(CityWeatherTemperatureUpdated e)
        {
            // HACK No listeners on OnTemperatureUpdated, no need to raise events
            Volatile.Read(ref OnTemperatureUpdated)?.Invoke(e);
        }

        public Task StartAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Scheduler running.");

            _timer = new Timer(DoWork, null, TimeSpan.Zero, _settings.SchedulerPeriod);

            return Task.CompletedTask;
        }

        private void DoWork(object state)
        {
            if (_executing)
            {
                return;
            }

            _executing = true;
            try
            {
                if (executionCount % 10 == 0)
                {
                    GetCities().ConfigureAwait(false).GetAwaiter().GetResult();
                }

                GetCitiesWeather().ConfigureAwait(false).GetAwaiter().GetResult();
            }
            finally
            {
                _executing = false;
            }

            executionCount++;

            _logger.LogInformation(
                "Scheduler is working. Count: {Count}", executionCount);
        }

        private async Task GetCities()
        {
            try
            {
                _logger.LogInformation("Starting GetCities");

                var apiCities = (await _wrapper.ApiCities()).ToList();
                var dbCities = await _queryBus.Send<GetAllCitiesQuery, List<City>>(new GetAllCitiesQuery()).ConfigureAwait(false);

                foreach (var city in apiCities)
                {
                    if (!dbCities.Exists(c => c.Name == city))
                    {
                        await _commandBus.Send<AddCityCommand>(new AddCityCommand { City = city }).ConfigureAwait(false);
                    }
                }

                foreach (var city in dbCities)
                {
                    if (!apiCities.Exists(c => c == city.Name))
                    {
                        await _commandBus.Send<DeleteCityCommand>(new DeleteCityCommand { CityId = city.Id }).ConfigureAwait(false);
                    }
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Failed executing GetCities");
            }
        }

        private async Task GetCitiesWeather()
        {
            try
            {
                _logger.LogInformation("Starting GetCitiesWeather");

                var dbCities = await _queryBus.Send<GetAllCitiesQuery, List<City>>(new GetAllCitiesQuery()).ConfigureAwait(false);

                foreach (var city in dbCities)
                {
                    var apiCityWeather = await _wrapper.ApiWeather(city.Name);

                    var cityWeather = await _queryBus.Send<GetCityWeatherQuery, CityWeatherViewModel>(new GetCityWeatherQuery { CityId = city.Id }).ConfigureAwait(false);
                    if (cityWeather == null)
                    {
                        await _commandBus.Send<AddCityWeatherCommand>(new AddCityWeatherCommand
                        {
                            CityId = city.Id,
                            CityName = city.Name,
                            Info = new CityWeatherInfo
                            {
                                Precipitation = apiCityWeather.Precipitation,
                                Temperature = apiCityWeather.Temperature,
                                Weather = apiCityWeather.Weather
                            }
                        }).ConfigureAwait(false);
                    }
                    else
                    {
                        _logger.LogInformation($"Scheduler.GetCitiesWeather updating CityWeather {city.Name}");
                        await _commandBus.Send<UpdateCityWeatherCommand>(new UpdateCityWeatherCommand
                        {
                            Id = cityWeather.Id,
                            Info = new CityWeatherInfo
                            {
                                Precipitation = apiCityWeather.Precipitation,
                                Temperature = apiCityWeather.Temperature,
                                Weather = apiCityWeather.Weather
                            }
                        }).ConfigureAwait(false);
                    }
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Failed executing GetCitiesWeather");
            }
        }

        public Task StopAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Scheduler is stopping.");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}
