﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Weather.Application.CityWeathers.Queries.GetCityWeather;
using Weather.Application.Entities;
using Weather.BackendApi.Dto;
using Weather.Domain.Queries;

namespace Weather.BackendApi.Controllers
{
    [ApiController]
    [Route("api")]
    public class CityController : ControllerBase
    {
        private readonly ILogger<CityController> _logger;
        private readonly IQueryBus _queryBus;

        public CityController(ILogger<CityController> logger, IQueryBus queryBus)
        {
            _logger = logger;
            _queryBus = queryBus;
        }

        /// <summary>
        /// Returns list of existing cities with corresponding ids.
        /// </summary>
        [HttpGet("Cities", Name = "Cities")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(CityDto[]), StatusCodes.Status200OK)]
        public async Task<ActionResult<CityDto[]>> Cities([FromQuery]string[] cities)
        {
            var dbCities = await _queryBus.Send<GetAllCitiesQuery, List<City>>(new GetAllCitiesQuery()).ConfigureAwait(false);
            var response = new List<CityDto>();

            foreach (var city in cities)
            {
                var c = dbCities.FirstOrDefault(c => c.Name == city);
                if (c != null)
                {
                    response.Add(new CityDto { Id = c.Id, Name = c.Name });
                }
            }

            return new ActionResult<CityDto[]>(response.ToArray());
        }
    }
}
