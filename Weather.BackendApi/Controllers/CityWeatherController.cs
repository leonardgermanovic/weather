﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EventStore.ClientAPI;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Converters;
using Weather.Application.CityWeathers.Events;
using Weather.Application.CityWeathers.Queries.GetCityWeather;
using Weather.BackendApi.Dto;
using Weather.Domain.Events;
using Weather.Domain.Queries;

namespace Weather.BackendApi.Controllers
{
    [ApiController]
    [Route("api")]
    public class CityWeatherController : ControllerBase
    {
        private readonly ILogger<CityController> _logger;
        private readonly IQueryBus _queryBus;

        public CityWeatherController(ILogger<CityController> logger, IQueryBus queryBus)
        {
            _logger = logger;
            _queryBus = queryBus;
        }

        /// <summary>
        /// Returns latest city weather by city id.
        /// </summary>
        [HttpGet("CityWeather/{cityId}", Name = "CityWeather")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(CityWeatherViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<CityWeatherViewModel>> CityWeather(Guid cityId)
        {
            var item = await _queryBus.Send<GetCityWeatherQuery, CityWeatherViewModel>(new GetCityWeatherQuery { CityId = cityId }).ConfigureAwait(false);

            if (item == null)
            {
                return NotFound();
            }

            return item;
        }

        private AggregateVersion[] _versions;
        private readonly Queue<CityWeatherTemperatureUpdated> _TemperatureUpdatedEventsQueue = new Queue<CityWeatherTemperatureUpdated>();

        [HttpGet("sse", Name = "SSE")]
        public async Task<IActionResult> Get([FromQuery]Guid[] id, [FromQuery]int[] v, CancellationToken cancellationToken)
        {
            if (Scheduler.Instance == null)
            {
                return BadRequest();
            }
            if (id == null || id.Length == 0 || v == null || v.Length == 0 || id.Length != v.Length)
            {
                return BadRequest();
            }
            
            _versions = new AggregateVersion[id.Length];
            for (var i = 0; i < id.Length; i++)
            {
                _versions[i] = new AggregateVersion { Id = id[i], Version = v[i] };
            }

            //Response.StatusCode = 200;
            var response = this.Response;           
            response.Headers.Add("Content-Type", "text/event-stream");

            Scheduler.Instance.OnTemperatureUpdated += TemperatureUpdatedEventListener;
            _logger.LogInformation($"TemperatureChangedEventListener subscribed to OnTemperatureChanged");

            var conn = CityWeatherEventHandler.CreateConnection();
            try
            {
                foreach (var version in _versions)
                {
                    StreamEventsSlice temperatureStream = await conn.ReadStreamEventsForwardAsync(version.Id.ToString(), Convert.ToInt64(version.Version)+1, 1000, false);
                    foreach (ResolvedEvent resolvedEvent in temperatureStream.Events)
                    {
                        var json = Encoding.UTF8.GetString(resolvedEvent.Event.Data);
                        var e = ConvertToCityWeatherTemperatureUpdated(json, resolvedEvent.Event.EventType);
                        version.Version = e.Version;
                        await WriteTemperatureUpdatedData(e, "ES");
                    }
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Failed in loop reading/converting events");
                throw;
            }
            finally
            {
                conn.Close();
            }

            while (!cancellationToken.IsCancellationRequested)
            {
                while (_TemperatureUpdatedEventsQueue.Count > 0)
                {
                    var e = _TemperatureUpdatedEventsQueue.Dequeue();
                    _logger.LogInformation($"TemperatureChangedEventListener Dequeue city {e.City} Queue.Count {_TemperatureUpdatedEventsQueue.Count}");
                    if (_versions != null)
                    {
                        var lastVersion = _versions.FirstOrDefault(x => x.Id == e.Id);
                        if (lastVersion == null)
                        {
                            await WriteTemperatureUpdatedData(e, "EVENT");
                        }
                        else if (lastVersion.Version < e.Version)
                        {
                            _versions = _versions.Where(x => x.Id != e.Id).ToArray();
                            if (_versions.Length == 0) _versions = null;
                            await WriteTemperatureUpdatedData(e, "EVENT");
                        }
                    }
                    else
                    {
                        await WriteTemperatureUpdatedData(e, "EVENT");
                    }
                }

                await Task.Delay(100);
            }

            Scheduler.Instance.OnTemperatureUpdated -= TemperatureUpdatedEventListener;
            _logger.LogInformation($"TemperatureChangedEventListener unsubscribed to OnTemperatureChanged");
            return Ok();
        }

        private void TemperatureUpdatedEventListener(CityWeatherTemperatureUpdated e)
        {
            if (_versions.Any(x => x.Id == e.Id))
            {
                this._TemperatureUpdatedEventsQueue.Enqueue(e);
                _logger.LogInformation($"TemperatureChangedEventListener Enqueue city {e.City}");
            }
        }

        private async Task WriteTemperatureUpdatedData(CityWeatherTemperatureUpdated e, string source)
        {
            try
            {
                var response = this.Response;
                string txt = Encoding.UTF8.GetString(Utf8Json.JsonSerializer.Serialize(ConvertToCityWeatherViewModel(e, source)));
                //byte[] encodedBytes = System.Text.Encoding.Unicode.GetBytes(txt);
                //string encodedTxt = Convert.ToBase64String(encodedBytes);
                var updatedAt = DateTimeToString(e.UpdatedAt);
                _logger.LogInformation($"WriteTemperatureUpdatedData encodedTxt city {e.City} updateAt {updatedAt}");
                await response.WriteAsync($"data:{txt}\n\n").ConfigureAwait(false);
                await response.Body.FlushAsync();
            }
            catch (Exception ex)
            {
                
            }
        }

        private static string DateTimeToString(DateTime? value)
        {
            return value?.ToString("yyyy-MM-dd HH:mm:ss.fffffff", CultureInfo.InvariantCulture);
        }

        private static CityWeatherViewModel ConvertToCityWeatherViewModel(CityWeatherTemperatureUpdated e, string source)
        {
            return new CityWeatherViewModel
            {
                Id = e.Id,
                City = e.City,
                Temperature = e.Temperature,
                UpdatedAt = new DateTimeOffset(e.UpdatedAt.Ticks, TimeSpan.Zero),
                Version = e.Version,
                Source = source
            };
        }

        private CityWeatherTemperatureUpdated ConvertToCityWeatherTemperatureUpdated(string json, string eventType)
        {
            switch (eventType)
            {
                case "CityWeatherCreated":
                    {
                        var e = Utf8Json.JsonSerializer.Deserialize<CityWeatherCreated>(json);
                        return new CityWeatherTemperatureUpdated(e.Id, e.Version, e.UpdatedAt, e.CityName, e.Temperature);
                    }
                case "CityWeatherUpdated":
                    {
                        var e = Utf8Json.JsonSerializer.Deserialize<CityWeatherUpdated>(json);
                        return new CityWeatherTemperatureUpdated(e.Id, e.Version, e.UpdatedAt, e.CityName, e.Temperature);
                    }
                default:
                    throw new NotSupportedException($"Not supported convertion from event type {eventType}");
            }
        }
    }

    public class AggregateVersion
    {
        public Guid Id { get; set; }
        public int Version { get; set; }
    }
}
