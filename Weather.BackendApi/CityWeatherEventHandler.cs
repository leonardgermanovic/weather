﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EventStore.ClientAPI;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Weather.Application.CityWeathers.Events;
using Weather.Domain.Events;

namespace Weather.BackendApi
{
    public class CityWeatherEventHandler : IEventHandler<CityWeatherCreated>, IEventHandler<CityWeatherUpdated>
    {
        private readonly ILogger<CityWeatherEventHandler> _logger;
        public CityWeatherEventHandler(ILogger<CityWeatherEventHandler> logger)
        {
            _logger = logger;
        }

        public static IEventStoreConnection CreateConnection()
        {
            var conn = EventStoreConnection.Create(new Uri("tcp://admin:changeit@localhost:1113"), "weather");
            conn.ConnectAsync().Wait();
            return conn;
        }

        public static EventData GetEventData<T>(T notification, Guid eventId) where T : IEvent
        {
            var json = Utf8Json.JsonSerializer.Serialize(notification);
            var eventType = typeof(T).Name;
            return new EventData(eventId, eventType, true, json, null);
        }

        private async Task<bool> AppendEventToStream<T>(T notification, Guid eventId, int version) where T : IEvent
        {
            var conn = CreateConnection();
            try
            {
                var eventData = GetEventData<T>(notification, eventId);
                WriteResult append = await conn.AppendToStreamAsync(eventId.ToString(), version - 1, eventData).ConfigureAwait(false);
                return true;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Failed to append event to stream");
                return false;
            }
            finally
            {
                conn.Close();
            }
        }

        public async Task Handle(CityWeatherCreated notification, CancellationToken cancellationToken)
        {
            var saved = await AppendEventToStream<CityWeatherCreated>(notification, notification.Id, notification.Version);
            if (saved && Scheduler.Instance != null)
            {
                _logger.LogInformation($"CityWeatherEventHandler CityWeatherCreated event city {notification.CityName}");
                Scheduler.Instance.TemperatureUpdated(ConvertToTemperatureUpdated(notification));
            }
        }

        public async Task Handle(CityWeatherUpdated notification, CancellationToken cancellationToken)
        {
            var saved = await AppendEventToStream<CityWeatherUpdated>(notification, notification.Id, notification.Version);
            if (Scheduler.Instance != null)
            {
                _logger.LogInformation($"CityWeatherEventHandler CityWeatherUpdated event city {notification.CityName}");
                Scheduler.Instance.TemperatureUpdated(ConvertToTemperatureUpdated(notification));
            }
        }

        public CityWeatherTemperatureUpdated ConvertToTemperatureUpdated(CityWeatherCreated e)
        {
            return new CityWeatherTemperatureUpdated(e.Id, e.Version, e.UpdatedAt, e.CityName, e.Temperature);
        }

        public CityWeatherTemperatureUpdated ConvertToTemperatureUpdated(CityWeatherUpdated e)
        {
            return new CityWeatherTemperatureUpdated(e.Id, e.Version, e.UpdatedAt, e.CityName, e.Temperature);
        }
    }
}
