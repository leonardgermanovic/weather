﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Weather.WeatherApi.Dto;

namespace Weather.WeatherApi.Controllers
{
    [Route("api")]
    [ApiController]
    public class WeatherController : ControllerBase
    {
        private static double _Temperature = 18;
        /// <summary>
        /// Returns weather data for specific city.
        /// </summary>
        /// <param name="Authorization">Token received from authorization endpoint. Provide it as "bearer {token}".</param>
        /// <param name="city">City from Cities endpoint.</param> 
        [HttpGet("Weather/{city}", Name = "Weather")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(CityWeather), StatusCodes.Status200OK)]
        public ActionResult<CityWeather> Get([FromHeader]string Authorization, string city)
        {
            _Temperature = Math.Round(_Temperature + 0.1, 1);
            return new CityWeather { City = city, Temperature = _Temperature, Precipitation = 50, Weather = "Normal"};
        }
    }
}
