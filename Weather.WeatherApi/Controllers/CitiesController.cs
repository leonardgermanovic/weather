﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Weather.WeatherApi.Controllers
{
    [Route("api")]
    [ApiController]
    public class CitiesController : ControllerBase
    {
        /// <summary>
        /// Returns list of cities for querying weather data.
        /// </summary>
        /// <param name="Authorization">Token received from authorization endpoint. Provide it as "bearer {token}".</param>
        [HttpGet("Cities", Name = "Cities")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(string[]), StatusCodes.Status200OK)]
        public ActionResult<string[]> Get([FromHeader]string Authorization)
        {
            return new List<string>() { "Vilnius", "Riga", "Tallin" }.ToArray();
        }
    }
}
