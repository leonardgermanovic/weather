﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Weather.WeatherApi.Dto;

namespace Weather.WeatherApi.Controllers
{
    [Route("api")]
    [ApiController]
    public class AutorizationController : ControllerBase
    {
        /// <summary>
        /// Returns token for authorizing other requests.
        /// </summary>
        /// <param name="request">Use username and password that were provided for you.</param> 
        [HttpPost("authorize", Name = "Authorize")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(AuthorizationResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<AuthorizationResponse> Authorize(AuthorizationRequest request)
        {
            if (request == null)
            {
                return BadRequest();
            }

            if (request.Username != "test" && request.Password != "test")
            {
                return BadRequest();
            }

            return new AuthorizationResponse { Bearer = Guid.Empty.ToString() };
        }
    }
}
