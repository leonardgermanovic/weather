﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Weather.WeatherApi.Dto
{
    public class AuthorizationRequest
    {
        [Required]
        [JsonProperty(Required = Required.DisallowNull)]
        public string Username { get; set; } = default!;

        [Required]
        [JsonProperty(Required = Required.DisallowNull)]
        public string Password { get; set; } = default!;
    }
}
