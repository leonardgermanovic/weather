﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Weather.WeatherApi.Dto
{
    public class AuthorizationResponse
    {
        [JsonProperty(Required = Required.DisallowNull)]
        public string Bearer { get; set; } = default!;
    }
}
