﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Weather.WeatherApi.Dto
{
    public class CityWeather
    {
        public string City { get; set; } = default!;
        public double Temperature { get; set; }
        public int Precipitation { get; set; }
        public string Weather { get; set; } = default!;
    }
}
