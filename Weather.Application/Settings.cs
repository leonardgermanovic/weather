﻿using System;

namespace Weather.Application
{
    public class Settings
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string WeatherApiUrl { get; set; }
        public string BackendApiUrl { get; set; }
        public string WeatherDatabase { get; set; }
        public TimeSpan SchedulerPeriod { get; set; }
        public EventStore EventStore { get; set; }
    }

    public class EventStore
    {
        public string ConnectionString { get; set; }
        public string Schema { get; set; }
    }
}
