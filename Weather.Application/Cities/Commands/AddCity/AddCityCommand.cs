﻿using MediatR;
using Weather.Domain.Commands;
using Weather.Domain.Queries;

namespace Weather.Application.Cities.Commands.AddCity
{
    public class AddCityCommand : ICommand
    {
        public string City { get; set; }
    }
}
