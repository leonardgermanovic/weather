﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Weather.Application.Entities;
using Weather.Domain.Commands;
using Weather.Persistence.Data;

namespace Weather.Application.Cities.Commands.AddCity
{
    public class AddCityCommandHandler : ICommandHandler<AddCityCommand>
    {
        private readonly IUnitOfWork _uow;

        public AddCityCommandHandler(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public async Task<Unit> Handle(AddCityCommand request, CancellationToken cancellationToken)
        {
            var repoCity = _uow.GetRepositoryAsync<City>();
            var entity = new City
            {
                Name = request.City
            };

            await repoCity.AddAsync(entity, cancellationToken);
            _uow.SaveChanges();
            return Unit.Value;
        }
    }
}
