﻿using System;
using MediatR;
using Weather.Domain.Commands;

namespace Weather.Application.Cities.Commands.AddCity
{
    public class DeleteCityCommand : ICommand
    {
        public Guid CityId { get; set; }
    }
}
