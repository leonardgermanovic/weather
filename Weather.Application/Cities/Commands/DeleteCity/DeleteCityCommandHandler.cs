﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Weather.Application.Entities;
using Weather.Domain.Commands;
using Weather.Persistence.Data;

namespace Weather.Application.Cities.Commands.AddCity
{
    public class DeleteCityCommandHandler : ICommandHandler<DeleteCityCommand>
    {
        private readonly IUnitOfWork _uow;

        public DeleteCityCommandHandler(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public async Task<Unit> Handle(DeleteCityCommand request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                var repoCity = _uow.GetRepository<City>();
                repoCity.Delete(request.CityId);
                _uow.SaveChanges();
                return Unit.Value;
            });
        }
    }
}
