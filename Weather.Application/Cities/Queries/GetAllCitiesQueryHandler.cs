﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Weather.Application.Entities;
using Weather.Domain.Queries;
using Weather.Persistence.Data;

namespace Weather.Application.CityWeathers.Queries.GetCityWeather
{
    public class GetAllCitiesQueryHandler : IQueryHandler<GetAllCitiesQuery, List<City>>
    {
        private readonly IUnitOfWork _uow;

        public GetAllCitiesQueryHandler(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public async Task<List<City>> Handle(GetAllCitiesQuery request, CancellationToken cancellationToken)
        {
            var repoCity = _uow.GetRepositoryAsync<City>();
            var page = await repoCity.GetListAsync(null, null, null, size: int.MaxValue).ConfigureAwait(false);
            return page.Items.ToList();
        }
    }
}
