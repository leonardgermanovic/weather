﻿using System.Collections.Generic;
using MediatR;
using Weather.Application.Entities;
using Weather.Domain.Queries;

namespace Weather.Application.CityWeathers.Queries.GetCityWeather
{
    public class GetAllCitiesQuery : IQuery<List<City>>
    {
    }
}
