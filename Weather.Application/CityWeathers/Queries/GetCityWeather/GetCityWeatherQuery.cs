﻿using System;
using MediatR;
using Weather.Domain.Queries;

namespace Weather.Application.CityWeathers.Queries.GetCityWeather
{
    public class GetCityWeatherQuery : IQuery<CityWeatherViewModel>
    {
        public Guid CityId { get; set; }
    }
}
