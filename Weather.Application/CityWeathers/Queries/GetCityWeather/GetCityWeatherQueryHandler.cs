﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Weather.Application.Entities;
using Weather.Application.Exceptions;
using Weather.Domain.Queries;
using Weather.Persistence.Data;

namespace Weather.Application.CityWeathers.Queries.GetCityWeather
{
    public class GetCityWeatherQueryHandler : IQueryHandler<GetCityWeatherQuery, CityWeatherViewModel>
    {
        private readonly IUnitOfWork _uow;

        public GetCityWeatherQueryHandler(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public async Task<CityWeatherViewModel> Handle(GetCityWeatherQuery request, CancellationToken cancellationToken)
        {
            var repoCityWeather = _uow.GetRepositoryAsync<CityWeather>();
            var cityWeather = ConvertCityWeatherViewModel(await repoCityWeather.SingleAsync(w => w.City.Id == request.CityId, e => e.OrderByDescending(x => x.UpdatedAt), i => i.Include(x => x.City)).ConfigureAwait(false));

            //if (cityWeather == null)
            //{
            //    throw new NotFoundException(nameof(CityWeather), request.CityId);
            //}

            return cityWeather;
        }

        private CityWeatherViewModel ConvertCityWeatherViewModel(CityWeather item)
        {
            if (item == null)
            {
                return null;
            }

            return new CityWeatherViewModel
            {
                Id = item.Id,
                Version = item.Version,
                City = item.City?.Name,
                Temperature = item.Temperature,
                UpdatedAt = new System.DateTimeOffset(item.UpdatedAt, System.TimeSpan.Zero),
                Source = "DB"
            };
        }
    }
}
