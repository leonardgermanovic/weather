﻿using System;

namespace Weather.Application.CityWeathers.Queries.GetCityWeather
{
    public class CityWeatherViewModel
    {
        public Guid Id { get; set; }
        public int Version { get; set; }
        public string City { get; set; }
        public double Temperature { get; set; }
        public DateTimeOffset UpdatedAt { get; set; }
        public string Source { get; set; }
    }
}
