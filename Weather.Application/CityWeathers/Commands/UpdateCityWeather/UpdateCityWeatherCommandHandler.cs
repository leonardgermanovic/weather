﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Weather.Application.Cities.Commands.UpdateCityWeather;
using Weather.Application.Entities;
using Weather.Application.Exceptions;
using Weather.Domain.Commands;
using Weather.Domain.Events;
using Weather.Persistence.Data;

namespace Weather.Application.Cities.Commands.AddCityWeather
{
    public class UpdateCityWeatherCommandHandler : ICommandHandler<UpdateCityWeatherCommand>
    {
        private readonly IUnitOfWork _uow;
        private readonly IEventBus _eventBus;
        private readonly ILogger<UpdateCityWeatherCommandHandler> _logger;

        public UpdateCityWeatherCommandHandler(IUnitOfWork uow, IEventBus eventBus, ILogger<UpdateCityWeatherCommandHandler> logger)
        {
            _uow = uow;
            _eventBus = eventBus;
            _logger = logger;
        }

        public async Task<Unit> Handle(UpdateCityWeatherCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var repoCityWeather = _uow.GetRepositoryAsync<CityWeather>();
                var cityWeather = await repoCityWeather.SingleAsync(w => w.Id == request.Id, null, i => i.Include(x => x.City)).ConfigureAwait(false);

                if (cityWeather == null)
                {
                    throw new NotFoundException(nameof(CityWeather), request.Id);
                }

                if (cityWeather.Update(request.Info))
                {
                    await _eventBus.Publish(cityWeather.PendingEvents.ToArray());

                    repoCityWeather.UpdateAsync(cityWeather);
                    _uow.SaveChanges();
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Failed to handle UpdateCityWeatherCommand request");
            }
            return Unit.Value;
        }
    }
}
