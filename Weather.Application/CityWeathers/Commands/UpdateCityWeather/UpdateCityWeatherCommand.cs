﻿using System;
using MediatR;
using Weather.Application.Entities;
using Weather.Domain.Commands;

namespace Weather.Application.Cities.Commands.UpdateCityWeather
{
    public class UpdateCityWeatherCommand : ICommand
    {
        public Guid Id { get; set; }
        
        public CityWeatherInfo Info { get; set; }
}
}
