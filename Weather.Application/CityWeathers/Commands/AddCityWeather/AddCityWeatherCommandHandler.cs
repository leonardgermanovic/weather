﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using Weather.Application.Entities;
using Weather.Domain.Commands;
using Weather.Domain.Events;
using Weather.Persistence.Data;

namespace Weather.Application.Cities.Commands.AddCityWeather
{
    public class AddCityWeatherCommandHandler : ICommandHandler<AddCityWeatherCommand>
    {
        private readonly IUnitOfWork _uow;
        private readonly IEventBus _eventBus;
        private readonly ILogger<AddCityWeatherCommandHandler> _logger;

        public AddCityWeatherCommandHandler(IUnitOfWork uow, IEventBus eventBus, ILogger<AddCityWeatherCommandHandler> logger)
        {
            _uow = uow;
            _eventBus = eventBus;
            _logger = logger;
        }

        public async Task<Unit> Handle(AddCityWeatherCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var repoCityWeather = _uow.GetRepositoryAsync<CityWeather>();
                var cityWeather = new CityWeather
                {
                    CityId = request.CityId,
                    Precipitation = request.Info.Precipitation,
                    Temperature = request.Info.Temperature,
                    Weather = request.Info.Weather,
                    CreatedAt = DateTime.UtcNow,
                    UpdatedAt = DateTime.UtcNow,
                    City = new City { Id = request.CityId, Name = request.CityName }
                };
                cityWeather.Create();

                await _eventBus.Publish(cityWeather.PendingEvents.ToArray());
                cityWeather.City = null;
                await repoCityWeather.AddAsync(cityWeather, cancellationToken);
                _uow.SaveChanges();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Failed to handle AddCityWeatherCommand request");
            }
            return Unit.Value;
        }
    }
}
