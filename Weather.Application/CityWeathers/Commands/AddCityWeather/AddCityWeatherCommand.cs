﻿using System;
using MediatR;
using Weather.Application.Entities;
using Weather.Domain.Commands;

namespace Weather.Application.Cities.Commands.AddCityWeather
{
    public class AddCityWeatherCommand : ICommand
    {
        public Guid CityId { get; set; }

        public string CityName { get; set; }

        public CityWeatherInfo Info { get; set; }
    }
}
