﻿using System;
using System.Collections.Generic;
using System.Text;
using Weather.Application.Entities;
using Weather.Domain.Events;

namespace Weather.Application.CityWeathers.Events
{
    public class CityWeatherTemperatureUpdated : IEvent
    {
        public Guid Id { get; }
        public int Version { get; }
        public DateTime UpdatedAt { get; }
        public string City { get; }
        public double Temperature { get; }

        public CityWeatherTemperatureUpdated(Guid id, int version, DateTime updatedAt, string city, double temperature)
        {
            Id = id;
            Version = version;
            UpdatedAt = updatedAt;
            City = city;
            Temperature = temperature;
        }
    }
}
