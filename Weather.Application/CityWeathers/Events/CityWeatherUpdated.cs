﻿using System;
using System.Collections.Generic;
using System.Text;
using Weather.Application.Entities;
using Weather.Domain.Events;

namespace Weather.Application.CityWeathers.Events
{
    public class CityWeatherUpdated : IEvent
    {
        public Guid Id { get; }
        public int Version { get; }
        public DateTime UpdatedAt { get; }
        public Guid CityId { get; }
        public string CityName { get; }
        public double Temperature { get; }
        public int Precipitation { get; set; }
        public string Weather { get; set; }
        public CityWeatherUpdated()
        {
        }
        public CityWeatherUpdated(Guid id, int version, DateTime updatedAt, Guid cityId, string cityName, double temperature, int precipitation, string weather)
        {
            Id = id;
            Version = version;
            UpdatedAt = updatedAt;
            CityId = cityId;
            CityName = cityName;
            Temperature = temperature;
            Precipitation = precipitation;
            Weather = weather;
        }
    }
}
