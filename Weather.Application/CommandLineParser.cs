﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Weather.Application
{
    public class CommandLineParser
    {
        public List<string> ParseArguments(string[] args)
        {
            if (args.Length == 0 || args[0] != "--city")
            {
                throw new ArgumentException();
            }

            var cityArgs = string.Concat(args.Skip(1));
            List<string> cities = cityArgs.Split(',', StringSplitOptions.RemoveEmptyEntries).ToList();
            return cities;
        }
    }
}
