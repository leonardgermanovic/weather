﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Weather.Application.Entities
{
    public class City
    {
        public City()
        {
            CityWeathers = new HashSet<CityWeather>();
        }

        public Guid Id { get; set; }

        public string Name { get; set; }

        public ICollection<CityWeather> CityWeathers { get; private set; }
    }
}
