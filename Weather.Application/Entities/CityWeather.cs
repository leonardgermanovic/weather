﻿using System;
using System.Collections.Generic;
using Weather.Application.CityWeathers.Events;
using Weather.Domain.Events;

namespace Weather.Application.Entities
{
    public class CityWeather : EventSourcedAggregate
    {
        public Guid CityId { get; set; }

        public City City { get; set; }

        public double Temperature { get; set; }

        public int Precipitation { get; set; }

        public string Weather { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }

        public void Create()
        {
            if (Id == Guid.Empty) Id = Guid.NewGuid();
            Version = GetNextVersion();
            Append(new CityWeatherCreated(Id, Version, UpdatedAt, UpdatedAt, City.Id, City.Name, Temperature, Precipitation, Weather));
        }

        public bool Update(CityWeatherInfo info)
        {
            if (this.Temperature == info.Temperature && this.Precipitation == info.Precipitation && this.Weather == info.Weather)
            {
                return false;
            }

            UpdatedAt = DateTime.UtcNow;
            info.UpdatedAt = UpdatedAt;
            Version = GetNextVersion();

            //if (this.Temperature != info.Temperature)
            //{
            //    Append(new CityWeatherTemperatureUpdated(Id, 0, UpdatedAt, City.Name, info.Temperature));
            //}

            Temperature = info.Temperature;
            Precipitation = info.Precipitation;
            Weather = info.Weather;

            Append(new CityWeatherUpdated(Id, Version, UpdatedAt, City.Id, City.Name, info.Temperature, info.Precipitation, info.Weather));

            return true;
        }
    }
}
