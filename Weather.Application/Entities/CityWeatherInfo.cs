﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Weather.Application.Entities
{
    public class CityWeatherInfo
    {
        public double Temperature { get; set; }
        public int Precipitation { get; set; }
        public string Weather { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}
