﻿using Weather.BackendApi.Client.OpenAPIService;

namespace Weather.Application.Formatters
{
    public interface IFormatter<T> where T : class
    {
        string Format(T item);
    }

    public class CityWeatherFormatter : IFormatter<CityWeatherViewModel>
    {
        public string Format(CityWeatherViewModel item)
        {
            if (item == null)
            {
                return string.Empty;
            }
            return string.Format($"{item.City} {item.Temperature} °C");
        }
    }
}
