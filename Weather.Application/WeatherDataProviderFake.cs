﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Weather.WeatherApi.Client.OpenAPIService;

namespace Weather.Application
{
    public class WeatherDataProviderFake : IClient
    {
        private string _bearer;
        private readonly List<string> _cities = new List<string> { "Vilnius", "Riga", "Tallinn" };
        private readonly List<CityWeather> _citiesWeather = new List<CityWeather>
        {
            new CityWeather { City = "Vilnius", Precipitation = 0, Temperature = 29, Weather = "Sunny" },
            new CityWeather { City = "Riga", Precipitation = 1, Temperature = 25, Weather = "Clouds" },
            new CityWeather { City = "Tallinn", Precipitation = 10, Temperature = 21, Weather = "Rains" },
        };

        public WeatherDataProviderFake() { }

        public Task<AuthorizationResponse> ApiAuthorizeAsync(AuthorizationRequest request)
        {
            return ApiAuthorizeAsync(request, CancellationToken.None);
        }

        public Task<AuthorizationResponse> ApiAuthorizeAsync(AuthorizationRequest request, CancellationToken cancellationToken)
        {
            _bearer = Guid.NewGuid().ToString();
            return Task.Run(() => new AuthorizationResponse { Bearer = _bearer }, cancellationToken);
        }

        public Task<ICollection<string>> ApiCitiesAsync(string authorization)
        {
            return ApiCitiesAsync(authorization, CancellationToken.None);
        }

        public Task<ICollection<string>> ApiCitiesAsync(string authorization, CancellationToken cancellationToken)
        {
            if (authorization != _bearer)
            {
                throw new Exception($"Not authenticated. Actual authorization is {_bearer}, provided is {authorization}.");
            }

            return Task.Run(() => (ICollection<string>)_cities, cancellationToken);
        }

        public Task<CityWeather> ApiWeatherAsync(string city, string authorization)
        {
            return ApiWeatherAsync(city, authorization, CancellationToken.None);
        }

        public Task<CityWeather> ApiWeatherAsync(string city, string authorization, CancellationToken cancellationToken)
        {
            if (authorization != _bearer)
            {
                throw new Exception($"Not authenticated. Actual authorization is {_bearer}, provided is {authorization}.");
            }

            if (!_cities.Contains(city))
            {
                throw new Exception($"City {city} is not supported");
            }

            var weather = _citiesWeather.Find(w => w.City == city);
            if (weather == null)
            {
                throw new Exception($"No data for city {city}");
            }

            return Task.Run(() => weather, cancellationToken);
        }
    }
}
