﻿using AutoMapper;
using Weather.Application.Entities;

namespace Weather.Application
{
    public class WeatherProfile : Profile
    {
        public WeatherProfile()
        {
            CreateMap<CityWeather, CityWeathers.Queries.GetCityWeather.CityWeatherViewModel>();
        }
    }
}
