﻿using System.IO;
using System.Reflection;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace Weather.Application
{
    public class SettingsProvider
    {
        public T Load<T>(string path) where T : class
        {
            var fullPath = GetFullPath(path);
            using (StreamReader reader = File.OpenText(fullPath))
            {
                var deserializer = new DeserializerBuilder()
                    .WithNamingConvention(new CamelCaseNamingConvention())
                    .Build();

                var settings = deserializer.Deserialize<T>(reader);
                return settings;
            }
        }

        public static string GetFullPath(string path)
        {
            var currentDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            //var currentDir = Directory.GetCurrentDirectory();
            var combinedPath = Path.Combine(currentDir, path);
            return Path.GetFullPath(combinedPath);
        }
    }
}
