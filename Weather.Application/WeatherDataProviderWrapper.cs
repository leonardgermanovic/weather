﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Weather.Application.Entities;
using OpenAPI = Weather.WeatherApi.Client.OpenAPIService;

namespace Weather.Application
{
    public interface IWeatherDataProviderWrapper
    {
        Task<ICollection<string>> ApiCities();

        Task<CityWeather> ApiWeather(string city);
    }

    public class WeatherDataProviderWrapper : IWeatherDataProviderWrapper
    {
        private readonly Settings _settings;
        private readonly OpenAPI.IClient _client;
        private string _bearer;
        public WeatherDataProviderWrapper(Settings settings, OpenAPI.IClient client)
        {
            _settings = settings;
            _client = client;
        }

        private Task<OpenAPI.AuthorizationResponse> ApiAuthorizeAsync()
        {
            return _client.ApiAuthorizeAsync(new OpenAPI.AuthorizationRequest
            {
                Username = _settings.Username,
                Password = _settings.Password
            });
        }

        public async Task<ICollection<string>> ApiCities()
        {
            await AuthenticateAsync();
            return await _client.ApiCitiesAsync(_bearer);
        }

        public async Task<CityWeather> ApiWeather(string city)
        {
            await AuthenticateAsync();
            return ConvertCityWeather(await _client.ApiWeatherAsync(city, _bearer).ConfigureAwait(false));
        }

        private CityWeather ConvertCityWeather(OpenAPI.CityWeather item)
        {
            return new CityWeather
            {
                City = new City { Name = item.City },
                Precipitation = item.Precipitation,
                Temperature = item.Temperature,
                Weather = item.Weather
            };
        }

        private async Task AuthenticateAsync()
        {
            if (_bearer == null)
            {
                //var authTask = ApiAuthorizeAsync();
                //authTask.Wait();
                //_bearer = authTask.Result.Bearer;
                var auth = await ApiAuthorizeAsync().ConfigureAwait(false);
                _bearer = auth.Bearer;
            }
        }
    }
}
